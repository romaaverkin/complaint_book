<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessagesTag extends Model
{
    use HasFactory;

    protected $table = 'messages_tags';
    protected $guarded = false;
}
