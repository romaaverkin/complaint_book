<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages_tags', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('message_id');
            $table->unsignedBigInteger('tag_id');

            $table->index('message_id', 'message_tag_message_idx');
            $table->index('tag_id', 'message_tag_tag_idx');

            $table->foreign('message_id', 'message_tag_message_fk')->on('messages')->references('id');
            $table->foreign('tag_id', 'message_tag_tag_fk')->on('tag')->references('id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages_tags');
    }
};
