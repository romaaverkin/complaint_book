<?php

use App\Http\Controllers\Admin\Category\CategoryController;
use App\Http\Controllers\Admin\Category\CreateController;
use App\Http\Controllers\Admin\Main\AdminController;
use App\Http\Controllers\Main\IndexController;
use Illuminate\Support\Facades\Route;


Route::group([], function () {
    Route::get('/', IndexController::class);
});

Route::group(['prefix' => 'admin'], function () {

    Route::group([], function () {
        Route::get('/', [AdminController::class, '__invoke']);
    });

    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', [CategoryController::class, '__invoke'])->name('admin.category.index');
        Route::get('/create', [CreateController::class, '__invoke'])->name('admin.category.create');
    });

});
